#!/bin/sh

#if [ -n "$WL_DISPLAY" ]; then
#  flatpak --user override --nosocket=wayland
#  flatpak --user override --socket=x11
##  export XDG_CURRENT_DESKTOP=dwm
##  systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
##  dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=river
#fi

BACKGROUND="$HOME/Pictures/Wallpapers/er nome tuo.png"

xrdb ~/.Xresources &
setxkbmap it &
# kill $(pidof slstatus); slstatus &
#nitrogen --restore &
feh --bg-fill "$BACKGROUND" &
pgrep -af polkit || /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
pidof st || tabbed_st &
pidof dunst || dunst &
#kill $(pidof xautolock); xautolock -time 10 -locker slock &
pidof picom || picom -b &
pidof sxhkd || sxhkd &
pidof firefox || firefox &
pidof thunderbird || thunderbird &
pidof thunar || thunar --daemon &
#pidof emacs || /usr/bin/emacs --daemon &
pidof xfce4-clipman || xfce4-clipman &
pidof nm-applet || nm-applet --indicator &

xset dpms 0 0 0
xset -dpms
# xset s 0 0
xset r rate 300 50

systemctl --user import-environment DISPLAY XAUTHORITY
if command -v dbus-update-activation-environment >/dev/null 2>&1; then
   dbus-update-activation-environment DISPLAY XAUTHORITY
fi

pgrep -f "bar\.sh" || $HOME/.local/bin/bar.sh
