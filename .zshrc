
# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' expand suffix
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=2
zstyle ':completion:*' original true
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/marcosti/.zshrc'

autoload -Uz compinit
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000000
SAVEHIST=10000000
setopt autocd extendedglob nomatch
setopt dotglob
#bindkey -v
# End of lines configured by zsh-newuser-install

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Fix backspace inconsistency
bindkey "^H" backward-delete-char
bindkey "^?" backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.

# History contextual completion
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey "^[OA" up-line-or-beginning-search
bindkey "^[OB" down-line-or-beginning-search

# zprofile
#source "$HOME/.zprofile"

# Syntax Highlighting
source $HOME/.git_repos/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $HOME/.git_repos/zsh-insulter/src/zsh.command-not-found

#
# ZSH_COMMAND_TIME: https://github.com/popstas/zsh-command-time/blob/master/command-time.plugin.zsh
#

# Configuration
# If command execution time above min. time, plugins will not output time.
ZSH_COMMAND_TIME_MIN_SECONDS=1

# Message to display (set to "" for disable).
ZSH_COMMAND_TIME_MSG="%s"

# Exclude some commands
ZSH_COMMAND_TIME_EXCLUDE=()

# Set only the LAST_COMMAND_TIME variable and don't print anything
ZSH_COMMAND_TIME_QUIET=1

ZSH_LAST_COMMAND_TIME=""

# Functions
_command_time_preexec() {
  # check excluded
  if [ -n "$ZSH_COMMAND_TIME_EXCLUDE" ]; then
    cmd="$1"
    for exc ($ZSH_COMMAND_TIME_EXCLUDE) do;
      if [ "$(echo $cmd | grep -c "$exc")" -gt 0 ]; then
        # echo "command excluded: $exc"
        return
      fi
    done
  fi

  timer=${timer:-$SECONDS}
  ZSH_COMMAND_TIME_MSG=${ZSH_COMMAND_TIME_MSG-"Time: %s"}
  ZSH_COMMAND_TIME_COLOR=${ZSH_COMMAND_TIME_COLOR-"white"}
  export ZSH_COMMAND_TIME=""
}

_command_time_precmd() {
  if [ $timer ]; then
    timer_show=$(($SECONDS - $timer))
    if [ -n "$TTY" ] && [ $timer_show -ge ${ZSH_COMMAND_TIME_MIN_SECONDS:-3} ]; then
      export ZSH_COMMAND_TIME="$timer_show"
      if [ ! -z ${ZSH_COMMAND_TIME_MSG} ]; then
        zsh_command_time
      fi
    fi
    unset timer
  fi
}

zsh_command_time() {
    if [ -n "$ZSH_COMMAND_TIME" ]; then
        hours=$(($ZSH_COMMAND_TIME/3600))
        min=$(($ZSH_COMMAND_TIME/60))
        sec=$(($ZSH_COMMAND_TIME%60))
        if [ "$ZSH_COMMAND_TIME" -le 60 ]; then
            timer_show="$fg[green]$ZSH_COMMAND_TIME s"
        elif [ "$ZSH_COMMAND_TIME" -gt 60 ] && [ "$ZSH_COMMAND_TIME" -le 180 ]; then
            timer_show="$fg[yellow]$min min $sec s"
        else
            if [ "$hours" -gt 0 ]; then
                min=$(($min%60))
                timer_show="$fg[red]$hours h $min min $sec s"
            else
                timer_show="$fg[red]$min min $sec s"
            fi
        fi
	ZSH_LAST_COMMAND_TIME="$(printf "${ZSH_COMMAND_TIME_MSG}\n" "$timer_show")"
	(( $ZSH_COMMAND_TIME_QUIET == 0 )) && printf "${ZSH_COMMAND_TIME_MSG}\n" "$timer_show"
    fi
}

#
# Prompt
#

segment_pwd() {
	# PWD, PWD path truncation
	local PWD="$(pwd)"
	PWD="$(echo $PWD | sed "s!$HOME!~!" | awk -F "/" '{\
		if (length($0) > 40){\
			for (i=1; i<NF; i++) {\
				if (i > 1)\
					printf "/";\
				if (length($i) > 5) {\
					printf "";\
				} else {\
					printf $i;\
				}\
			}\
			printf "/%s\n", $NF;\
		} else {\
			print $0;\
		}\
	}' )"
	echo "$PWD"
}
segment_time() {
	local TIME="$(echo $ZSH_LAST_COMMAND_TIME)"
	[ -n "$TIME" ] && TIME="%B%K{25} $TIME %k%b"
	echo "$TIME"
}

promptsetup() {
	local PWD="$(segment_pwd)"
	local TIME="$(segment_time)"

	# git current branch and status
	local GIT=""

	if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == "true" ]]; then
		local GIT_TOPLEVEL="$(git rev-parse --show-toplevel)"
		if [[ -d "$GIT_TOPLEVEL"/.git ]]; then
			GIT="$(cat "$GIT_TOPLEVEL"/.git/HEAD)"
			if [[ -n "$(echo "$GIT" | grep "^ref:")" ]]; then
				GIT="%B%K{red}  $(echo "$GIT" | sed 's!^.*/!!g') %b%k"
			else
				GIT="%B%K{red}  $(echo "$GIT" | cut -c -7) %b%k"
			fi
		else
			GIT="%B%K{red}  %b%k"
		fi
	fi

	PS1="%(?..%K{196} %B%?%b %k)${TIME}%B%K{9}%F{235}  %n@%m%b %k%f%B%K{25} ${PWD} %b%k${GIT}"$'\n'"%B%F{white}%K{235} %# %b%f%k "

	unset TIME
	unset ZSH_LAST_COMMAND_TIME
}

PROMPT_COMMAND="promptsetup"

precmd() {
	_command_time_precmd
	eval "$PROMPT_COMMAND"
}
preexec() {
	_command_time_preexec
  echo -ne '\e[5 q' 
}

#PS1="%(?..%K{197}[%?]%k )[ %B%F{green}%n@%m%b%f ] %B%F{blue}%(4~|...|)%3~%F{white} %# %b%f%k"
#PS1="%(?..%K{196} %B%?%b %k)%B%K{9}%F{235}  %n@%m%b %k%f%B%K{25} %~ %k"$'\n'"%F{white}%K{235} %# %b%f%k "

# Source: https://linuxhint.com/change-zsh-prompt-name/
# '%K' – This option tells the prompt to start in a different background color. It is similar to the %F. To set a color, set %K followed by the name of the color in curly braces
# '%n' – This displays the username
# '%m' – The system’s hostname up to the first ‘.’ You can add an integer value after the % to indicate how many hostname components you desire. If you want to display the full hostname, use the %M instead
# '%B' – Start Boldface mode
# '%F' – This is similar to the %K and starts in a different background mode. Similarly, you can pass the color inside a pair of curly braces in numerical or normal format

mcclone() {
  git clone "https://github.com/marco-04/$1"
}

cowfig() {
  figlet $1 | cowsay -n
}

direxplorer() {
  local levels=1
  local backwards=0
  local dmenu_fix=1
  local dmenu_bw=5
  local dmenu_z=500
  local reverse=0
  local icons=1
  local colors=0
  local revert=0
  local curpath="$(pwd)"
  local gotopath="$(pwd)"
  local usedmenu=1
  local fm=0
  local dmenuprompt=""

  # Parse arguments
  for ((i=1; i<=$#; i++))
  do
    arg="${(P)i}"
    case "$arg" in
      "-a") dmenu_fix=0;; # Disable automatic detection of wayland session
      "-b") # Border width
        ((++i))
        if ((${(P)i} >= 0))
        then
          dmenu_bw=${(P)i}
        else
          return 1;
        fi;;
      "-B") backwards=1;; # Add backwards option
      "-c") colors=1;; # Enable colors (works only with fzf)
      "-f") usedmenu=0;; # Use fzf instead of dmenu
      "-i") icons=0;; # Disable icons
      "-l") # Number of times the function will iterate through the folders
        if ((levels >= 1))
        then
          ((++i))
          if [[ -n "${(P)i}" ]]
          then
            levels="${(P)i}"
          else
            return 1
          fi
        fi;;
      "-L") levels=99999999; fm=1; backwards=1;; # "Unlimited" iterations (cancelling selection will quit), implies `backwards`, useless with `revert`
      "-r") reverse=1;; # Reverse output
      "-R") revert=1;; # Return to cwd if cancelling the choice
      "-p") # Prompt
        ((++i))
        if [[ -n "${(P)i}" ]]
        then
          dmenuprompt="${(P)i}"
        else
          return 1;
        fi;;
      *) # Check if the unknown argument is a directory, otherwise exit
        if [[ -d "$arg" ]]
        then
          curpath="$arg"
        else
          return 255 # Fallback when an unknown argument is passed
        fi;;
    esac
  done

  ((colors)) && ((usedmenu)) && return 1

  # Automatically detect if dmenu_bw has to be 0 or not
  if [[ "$XDG_SESSION_DESKTOP" == "river" ]] && ((dmenu_fix))
  then
    dmenu_bw=0
  fi

  local choice=""
  local ezaresults=""
  local iconarg="--icons=never"
  local colorarg="--color=never"
  local reversearg=""
  local hiddenfilesarg=""
  ((icons)) && iconarg="--icons=always"
  ((colors)) && colorarg="--color=always"
  ((reverse)) && reversearg="-r"
  ((backwards)) && hiddenfilesarg="-aa"

  for ((i=0; i<$levels; i++))
  do
    ezaresults=$(eza -1 -D --no-quotes $iconarg $colorarg $reversearg $hiddenfilesarg $curpath) # Gets the contents of the `curpath` folder
    # ((reverse)) && ezaresults=$(echo $ezaresults | sort -r) # Sorts the contents of `curpath` in reverse
    if ((usedmenu))
    then
      # Use dmenu
      choice=$(echo $ezaresults | dmenu -bw $dmenu_bw -c -i -l 10 -p "${dmenuprompt:-Choose:}")
    else
      # Use fzf
      choice=$(echo $ezaresults | fzf --ansi --prompt "${dmenuprompt:-Choose:}")
    fi

    # If icons are enabled, they need to be removed before interpolating the selected folder
    ((icons)) && choice="$(echo "$choice" | cut -d ' ' -f 1 --complement)"
    if [[ -n "$choice" ]]
    then
      # Set `curpath` to the new selected folder to be reused/cd-ed into
      curpath="$curpath/$choice"
      ((backwards)) && [[ "$choice" == "." ]] && break # Breaks when selecting `.`
      ((backwards)) && [[ "$choice" == ".." ]] && ((i-=2)) # Step back in level counting
    else
      # If the choice is cancelled, revert curpath to the cwd
      ((revert)) && curpath="$gotopath"
      break
    fi
  done
  gotopath="$curpath"
  cd "$gotopath"
}

# Edit a script in PATH
edit() {
  local script="$1"
  $EDITOR "$(which "$script")"
}

maradonamake() {
  MARADONAMAKE="$HOME/Documents/MKWii/MaradonaKart"
  if [[ -f "$MARADONAMAKE/maradonamake" ]]
  then
    if [[ "$1" == "chdir" ]]
    then
      local dir="$($MARADONAMAKE/maradonamake $@ | sed 's/^cd //')"
      [[ -d "$dir" ]] && cd "$dir"
    else
      $MARADONAMAKE/maradonamake $@
    fi
  else
    notify-send "SEI UN COGLIONE" "MONTA L'SSD TESTA DI CAZZO"
    return 255
  fi
}

open() {
  local parameters="$@"
  [[ -z "$parameters" ]] && xdg-open . || xdg-open $parameters
}

dmenu_bw() {
  env BW_SESSION="$(gpg -qr marcosti096@duck.com -d /home/marcosti/.gnupg/bw_session_token.gpg)" dmenu_bw
}

# Aliases
alias shit="shutdown -h now"
alias arduinosucks="sudo modprobe -r cdc_acm && sudo modprobe cdc_acm && sudo chmod 666 /dev/ttyACM0"
alias yoink="doas pacman -S"
alias yoinks="paru -Ss"
alias yeet="doas pacman -R"
alias syum="doas pacman -Syu"
alias syyum="doas pacman -Syy"
alias ls="ls --color=auto"
alias exa="eza -la --color=automatic --icons=automatic"
alias eza="eza -la --color=automatic --icons=automatic"
alias urgent="xdotool selectwindow -- set_window --urgency 1"
alias mp3-dl="yt-dlp -i --extract-audio --audio-format mp3 --audio-quality 0"
alias qtfix="paru -S --rebuild --batchinstall --sudoloop=-L --skipreview --noconfirm qt5-styleplugins qt6gtk2"
alias acli="arduino-cli"
# alias zitto="timeout 5 ssh poul@pornhub.lan.poul.org '/home/poul/restart.sh'"
alias alessio="timeout 5 ssh poul@poulumio pamixer -t"
alias poulumio="PULSE_SERVER=poulumio pulsemixer"

alias config='git --git-dir=$HOME/Documents/dev/repos/configs --work-tree=$HOME'

alias docs="direxplorer -p Documents: $HOME/Documents"
alias devproj="direxplorer -p Project: $HOME/Documents/dev/repos"
alias polimi="direxplorer -l 2 -r -p Polimi: $HOME/Documents/Polimi"
alias poul="direxplorer -p POuL: $HOME/.poul"
alias repos="direxplorer -p 'GIT Repos:' $HOME/.git_repos"

alias zshrc="${EDITOR:=nvim} $HOME/.zshrc"
alias reload=". $HOME/.zshrc"

eval $(thefuck --alias)

# Startup
clear -x
# neofetch
# neofetch --ascii "$(tput bold; tput setaf 6; cat ~/Documents/ASCIIArt/moai3.txt)"
fastfetch

if [ -e /home/marcosti/.nix-profile/etc/profile.d/nix.sh ]; then . /home/marcosti/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# `batman` zsh completion
command -v batman >/dev/null && compdef _man batman

# Begin: PlatformIO Core completion support
# eval "$(_PIO_COMPLETE=zsh_source pio)"
# End: PlatformIO Core completion support

