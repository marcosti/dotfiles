#!/usr/bin/env bash

# ^c$var^ = fg color
# ^b$var^ = bg color

interval=0

black=#222222
red=#e9653b
green=#39e9a8
orange=#e5b684
blue=#44aae6
pink=#e17599
cyan=#3dd5e7
white=#c3dde1

lblack=#5f6368
lred=#ee6655
lgreen=#00ff9a
lorange=#e89440
lblue=#009afb
lpink=#ff578f
lcyan=#efffff
lwhite=#d9fbff

cpu() {
  cpu_val=$(grep -o "^[^ ]*" /proc/loadavg)

  printf "^c$black^^b$lblue^ CPU"
  printf "^c$black^ ^b$blue^ $cpu_val"
}

battery() {
  BATPATH="/sys/class/power_supply/BAT0"
  get_capacity="$(cat "$BATPATH/capacity")"
  declare -a battery_icons=( "󰂃" "󰁺" "󰁻" "󰁼" "󰁽" "󰁾" "󰁿" "󰂀" "󰂁" "󰂂" "󰁹" )
  declare -a colors=( "$lred" "$green" "$blue" "$orange" )
  icon=$(echo "$get_capacity/10")
  status=$(sed 's/^Discharging$/0/;s/^Charging$/1/;s/^Full$/2/;s/^Not charging$/3/' "$BATPATH/status")
  printf "^c$black^^b${colors[${status:-2}]}^ ${battery_icons[$icon]} $get_capacity%%"
  # printf "^c$lorange^^b$black^ $get_capacity" 
}

mem() {
  printf "^c$black^^b$lblue^  "
  printf "^c$black^^b$blue^ $(free -h | awk '/^Mem/ { print $3 }' | sed s/i//g)"
}

clock() {
 printf "^c$black^^b$lorange^  "
 printf "^c$black^^b$orange^ $(date '+%H:%M:%S')"
}

bardate() {
 printf "^c$black^^b$lorange^ \ueab0 "
 printf "^c$black^^b$orange^ $(date "+%d/%m/%y") ^d^"
}

while true; do

  #[ $interval = 0 ] || [ $(($interval % 3600)) = 0 ] && updates=$(pkg_updates)
  interval=$((interval + 1))

  #sleep 0.25 && xsetroot -name "$updates $(battery) $(brightness) $(cpu) $(mem) $(wlan) $(clock)"
  sleep 0.25 && xsetroot -name "^d^ $(battery) $(clock) $(bardate) "
done
