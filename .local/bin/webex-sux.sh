#!/bin/env bash

IFS=$'\n'
trap "exit -1" SIGINT SIGTERM

NUCLEAR=0
CLEANUP=0
NOCONV=0
SHOWOPTS=0

usage() {
  printf "\e[1mUsage\e[0m:\n\twebex-sux.sh [\e[1m--cleanup\e[0m] [\e[1m--noconv\e[0m] [\e[1m--nuclear\e[0m] [\e[1m--showopts\e[0m] \e[1m\e[4mdestination\e[0m\n";
  exit 1;
}

for arg in $@
do
  [[ "$arg" == "-h" ]] && usage
  if [[ -n "$(echo "$arg" | grep "^--")" ]]; then
    case "$arg" in
      "--nuclear") NUCLEAR=1;;
      "--cleanup") CLEANUP=1;;
      "--noconv") NOCONV=1;;
      "--showopts") SHOWOPTS=1;;
      "--help") usage;;
      *) echo "Unknown argument"; usage;
    esac
  else
    if [[ -d "$arg" ]]; then
      if [[ -z "$DEST" ]]; then
        DEST=$(echo "$arg" | sed 's/\/*$//')
      else
        echo "DEST was already set"
        exit 3
      fi
    else
      echo "$arg is not a directory"
      exit 2
    fi
  fi
done

[[ -z "$DEST" ]] && DEST="."

(($SHOWOPTS)) && echo -e "NUCLEAR == $(($NUCLEAR))\nCLEANUP == $(($CLEANUP))\nNOCONV == $(($NOCONV))\nDEST == $DEST\n"

(($NUCLEAR)) && (($NOCONV)) && rm -v *.mp4 | lolcat
(($CLEANUP)) && rm -v *.mkv | lolcat

(($NOCONV)) && exit 0

declare -a FILES=( $(ls -1N *.mp4 2> /dev/null) )

NFILES=${#FILES[@]}
CNT=0

(($NFILES)) || echo "No mp4 files found" | lolcat

for file in ${FILES[@]}
do
  RET=0
  ((CNT++))

  OUTNAME=$(echo "$file" | sed "s/ /_/g;s/'//g;s/mp4$/mkv/" )

  echo -n "$(tput bold)$(tput setaf 2)[$CNT/$NFILES]$(tput sgr0) - "; echo "Converting $file to $OUTNAME..." | lolcat

  ffprobe -show_format "$file" 2>&1 | grep Duration | cut -d ' ' --fields=3,4 | sed 's/,//' | lolcat
  # ffmpeg -v warning -stats -y -i "$file" "$DEST/$OUTNAME" && echo "[  OK  ]" || echo "[ FAIL ]"
  ffpb -y -i "$file" -c:v copy "$DEST/$OUTNAME" && echo "$(tput bold)$(tput setaf 2)[  OK  ]$(tput sgr0)" || echo "$(tput bold)$(tput setaf 1)[ FAIL ]$(tput sgr0)"

  echo;

done

(($NUCLEAR)) && rm -v *.mp4 | lolcat

exit 0
