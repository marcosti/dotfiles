#!/usr/bin/env bash

[[ -z "$WALLPAPER" ]] && WALLPAPER="$HOME/Pictures/Wallpapers/er nome tuo.png"

declare -a conf=( $(find $HOME/.screenlayout -type f) )

choice="$(printf "%s\n" ${conf[@]} | dmenu -c -l 10 -p "Monitor:")"

${choice:?"No choice made"}
sleep 0.1
feh --bg-fill "$WALLPAPER"

