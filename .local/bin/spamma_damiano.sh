#!/bin/bash

WINDOW=8388618

echo -n "ALLARME TEST" | xclip -selection clipboard

while true
do
  xdotool key --window ${WINDOW:?"WINDOW not set"} ctrl+v
  sleep 0.1
  xdotool key --window ${WINDOW:?"WINDOW not set"} enter
  sleep 0.1
done
