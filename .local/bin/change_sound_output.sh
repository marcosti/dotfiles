#!/usr/bin/env bash

set -e

cur_sink="$(pactl get-default-sink)"
declare -i cur_sink_id

sinks="$(pactl -f json list sinks | jq '[ .[] | {id: .index, name: .name, description: .description} ]')"
declare -i sinks_amount=$(echo "$sinks" | jq 'length')

declare -a sinks_list=()

for ((i=0;i<$sinks_amount;i++))
do
  sinks_list+=( "$(echo "$sinks" | jq ".[$i].description" | sed 's/"//g')	$(echo "$sinks" | jq ".[$i].id")" )
  [[ "$cur_sink" == "$(echo "$sinks" | jq ".[$i].name" | sed 's/"//g')" ]] && cur_sink_id=$(echo "$sinks" | jq ".[$i].id")
done

dmenu_choice="$(printf "%s\n" "${sinks_list[@]}" | dmenu -c -i -l 10 -p "Output:")"
description="$(echo "$dmenu_choice" | cut -d '	' -f 1)"
id="$(echo "$dmenu_choice" | cut -d '	' -f 2)"

if ((cur_sink_id == id))
then
  notify-send "Sound output" "Sound output was not changed: \"$description\""
else
  pactl set-default-sink "${id?:"No choice was made"}"
  notify-send "Sound output" "Changed sound output to \"$description\""
fi
volume_notify
