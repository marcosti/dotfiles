#!/usr/bin/env bash

CACHEDIR="$HOME/.config/katex-ins"
LISTDIR="$CACHEDIR/cmds"
HISTDIR="$CACHEDIR/hist"

AW_FLAG="-w $(xdotool getactivewindow)"
DMENUFLAGS="-l 20 -p"
VAR=""

FN_INS=insmode
FN_TYPE=typemode
FN_CASES=casesmode

EXIT=0
ECHOMODE=0
ARGS=0

declare CURMODE
declare DMENUOUT
declare DMENUPROMPT
declare CMD
declare CURVAR
declare LINE

for ((i = 1; i <= $#; i++)); do
  case ${!i} in
    --echo) let ECHOMODE=1;;
    --prompt) ((i++)); VAR="${!i}";;
    *) echo "Flag non riconosciuta ${!i}"; exit 1;;
  esac
done

runcmd() {
  if [[ "$CURMODE" == "ins" || "$CURMODE" == "case" ]]; then
    DMENUOUT="$(cat "$LISTDIR"/* | dmenu -i $DMENUFLAGS $1 $AW_FLAG | sed 's/^.*\t//')"
  else
    DMENUOUT="$(cat "$LISTDIR"/functions | dmenu -i -F $DMENUFLAGS $1 $AW_FLAG | sed 's/^.*\t//')"
  fi
}

insmode() {
  CURMODE=ins
  DMENUPROMPT="[$VAR]INS:"
}
typemode() {
  CURMODE=type
  DMENUPROMPT="[$VAR]TYPE:"
}
casesmode() {
  CURMODE=case
  DMENUPROMPT="CASE:"
}

hasargs() {
  if [[ -n "$(echo "$DMENUOUT" | grep ";;")" ]]; then
    ARGS=1
  else
    ARGS=0
  fi
}

insmode
while (($EXIT != 1)); do
  runcmd $DMENUPROMPT
  if [[ -n "$(echo $DMENUOUT | grep "^MODE:")" ]]; then
    eval $(echo "$DMENUOUT" | sed 's/^MODE://')
    continue
  fi

  if [[ "$CURMODE" == "ins" && -z $DMENUOUT ]]; then
    if (($ECHOMODE == 0)); then
      EXIT=1
    fi
  elif [[ -z $DMENUOUT ]]; then
    if (($ECHOMODE == 0)); then
      insmode
      runcmd
    fi
  fi

  hasargs

  if [[ "$CURMODE" == "type" ]]; then
    if (($ECHOMODE == 1)); then
      echo "$DMENUOUT" | sed "s/'//g"
      EXIT=1
    else
      OUT=$(echo "$DMENUOUT" | sed 's//\\/g' | sed "s//'/g")
      echo -n "$OUT" | xclip -selection clipboard
      # xdotool type --delay 100 "$OUT"
      # xdotool getactivewindow key ctrl+v
      xdotool getactivewindow key ctrl+shift+v
      EXIT=1
    fi
  elif (( $ARGS == 1 )); then
    CMD="$(echo "$DMENUOUT" | awk -F ";;" '{ print $1 }')"
    LINE="$(echo "$DMENUOUT" | awk -F ";;" 'BEGIN {OFS = FS}{ $1=""; print }' | sed 's/^;;//')"
    #CMD="$(echo "$DMENUOUT" | sed 's/;;.*$//;s/\$/\\/g')"
    #LINE="$(echo "$DMENUOUT" | sed 's/^.*;;//')"
    i=0
    while [[ -n "$LINE" ]]; do
      if [[ -n "$(echo "$LINE" | grep ";;" )" ]]; then
        CURVAR="$(echo "$LINE" | awk -F ";;" '{ print $1 }')"
        LINE="$(echo "$LINE" | awk -F ";;" 'BEGIN {OFS = FS}{ $1=""; print }' | sed 's/^;;//')"
        #LINE="$(echo "$LINE" | sed 's/^.*;;//')"
      else
        CURVAR="$LINE"
        unset LINE
      fi

      OUTPUT="$($0 --echo --prompt "$CURVAR" | sed "s/'//g")"

      eval ${CURVAR}="$OUTPUT"
      OUT=$(eval echo "$CMD")
      ((i++))
      (($i == 5)) && break
    done
    if (($ECHOMODE == 1)); then
      eval echo $OUT
      EXIT=1
    else
      #eval echo "$OUT " | sed 's//\\/g' | xclip -selection clipboard
      OUT=$(eval echo "$OUT" | sed 's//\\/g' | sed "s//'/g")
      echo -n "$OUT" | xclip -selection clipboard
      # xdotool type --delay 100 "$OUT"
      # xdotool getactivewindow key ctrl+v
      xdotool getactivewindow key ctrl+shift+v
      EXIT=1
    fi
  elif (($ARGS == 0)); then
    if (($ECHOMODE == 1)); then
      echo "$DMENUOUT" | sed "s/'//g"
      EXIT=1
    else
      OUT=$(echo "$DMENUOUT" | sed 's//\\/g' | sed "s//'/g")
      echo -n "$OUT" | xclip -selection clipboard
      # xdotool type --delay 100 "$OUT"
      # xdotool getactivewindow key ctrl+v
      xdotool getactivewindow key ctrl+shift+v
      EXIT=1
    fi
  fi
done

