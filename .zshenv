export EDITOR="nvim"
export BAT_THEME="gruvbox-dark"
export PATH="$PATH:$HOME/.local/bin"
source "$HOME/.cargo/env"
fpath=($HOME/.local/share/zsh/completion $fpath)
